import Inferno from 'inferno';
import Component from 'inferno-component';

class MyComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0
    }
  }
  render() {
    return (
      <div>
        <span>Counter is at: { this.state.counter }</span>
        <button onClick={this.add}></button>
      </div>
    )
  }

  add = () => {
    this.setState({counter: this.state.counter + 1});
  }

}