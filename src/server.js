import Inferno from 'inferno';
import HomePage from './client/pages/HomePage';
import InfernoDOM from 'inferno-dom';
import { Router, Route, Link, browserHistory } from 'inferno-router';

InfernoDOM.render((
    <Router history={ browserHistory }>
        <Route path="/" component={ HomePage }>
        </Route>
    </Router>
), document.getElementById("app"));
